// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/UserDefinedEnum.h"
#include "ItemID.generated.h"

/**
 * 
 */
UENUM(BlueprintType)
enum class EItemID : uint8
{
	Claravoyance,
	Silence,
	Unlock,
	Invisibility,
	MagicRune,
	Hammer,
	TheivesHook,
	WoodPlank,
	Sealedbook,
	LockedBox
};
