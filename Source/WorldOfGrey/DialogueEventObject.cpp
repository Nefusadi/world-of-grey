// Fill out your copyright notice in the Description page of Project Settings.

#include "WorldOfGrey.h"
#include "DialogueEventObject.h"

void UDialogueEventObject::BeginMessage(UObject* WorldContextObject)
{
	OnBegin.Broadcast("");
}

void UDialogueEventObject::EndMessage(UObject* WorldContextObject, FName option)
{
	OnEnd.Broadcast(option);
}


