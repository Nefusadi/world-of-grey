// Fill out your copyright notice in the Description page of Project Settings.

#include "WorldOfGrey.h"
#include "ScreenshotLibrary.h"
#include "Runtime/ImageWrapper/Public/IImageWrapper.h"
#include "Runtime/ImageWrapper/Public/IImageWrapperModule.h"
#include "Runtime/Engine/Public/ImageUtils.h"
#include "Runtime/Core/Public/GenericPlatform/GenericPlatformDriver.h"

UTexture2D* UScreenshotLibrary::LoadScreenshot(const FString& filename)
{
	UTexture2D* LoadedT2D = NULL;

	IImageWrapperModule& ImageWrapperModule = FModuleManager::LoadModuleChecked<IImageWrapperModule>(FName("ImageWrapper"));

	TSharedPtr<IImageWrapper> ImageWrapper = ImageWrapperModule.CreateImageWrapper(EImageFormat::PNG);

	//Load From File
	TArray<uint8> RawFileData;
	if (!FFileHelper::LoadFileToArray(RawFileData, *(FPaths::ProjectSavedDir() + filename)))
	{
		return NULL;
	}

	//Create T2D!
	if (ImageWrapper.IsValid() && ImageWrapper->SetCompressed(RawFileData.GetData(), RawFileData.Num()))
	{
		const TArray<uint8>* UncompressedBGRA = NULL;
		if (ImageWrapper->GetRaw(ERGBFormat::BGRA, 8, UncompressedBGRA))
		{
			LoadedT2D = UTexture2D::CreateTransient(ImageWrapper->GetWidth(), ImageWrapper->GetHeight(), PF_B8G8R8A8);

			//Valid?
			if (!LoadedT2D)
			{
				return NULL;
			}

			//Copy!
			void* TextureData = LoadedT2D->PlatformData->Mips[0].BulkData.Lock(LOCK_READ_WRITE);
			FMemory::Memcpy(TextureData, UncompressedBGRA->GetData(), UncompressedBGRA->Num());
			LoadedT2D->PlatformData->Mips[0].BulkData.Unlock();

			//Update!
			LoadedT2D->UpdateResource();

		}
	}

	return LoadedT2D;
}

FString UScreenshotLibrary::GetScreenshotDir()
{
	return FPaths::ProjectSavedDir();
}

bool UScreenshotLibrary::SaveRenderTargetToDisk(UTextureRenderTarget2D* InRenderTarget, FString Filename)
{
	if (!IsValid(InRenderTarget))
		return false;

	FTextureRenderTargetResource *rtResource = InRenderTarget->GameThread_GetRenderTargetResource();
	FReadSurfaceDataFlags readPixelFlags(RCM_UNorm);

	TArray<FColor> outBMP;
	outBMP.AddUninitialized(InRenderTarget->GetSurfaceWidth() * InRenderTarget->GetSurfaceHeight());
	rtResource->ReadPixels(outBMP, readPixelFlags);
	
	for (FColor& color : outBMP)
	{
		color.A = 255;
	}

	FIntPoint destSize(InRenderTarget->GetSurfaceWidth(), InRenderTarget->GetSurfaceHeight());
	TArray<uint8> CompressedBitmap;
	FImageUtils::CompressImageArray(destSize.X, destSize.Y, outBMP, CompressedBitmap);
	bool imageSavedOk = FFileHelper::SaveArrayToFile(CompressedBitmap, *(FPaths::ProjectSavedDir() + Filename));

	return imageSavedOk;
}

bool UScreenshotLibrary::DeleteScreenshot(const FString& filename)
{
	return FPlatformFileManager::Get().GetPlatformFile().DeleteFile(*(FPaths::ProjectSavedDir() + filename));
}