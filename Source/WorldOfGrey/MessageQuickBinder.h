// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "DialogueEventObject.h"
#include "MessageQuickBinder.generated.h"

/**
 * 
 */
UCLASS()
class WORLDOFGREY_API UMessageQuickBinder : public UBlueprintAsyncActionBase
{
	GENERATED_BODY()

public:
	UMessageQuickBinder(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(BlueprintAssignable)
		FMessageEvent MessageBegin;

	UPROPERTY(BlueprintAssignable)
		FMessageEvent MessageEnd;

	UFUNCTION(BlueprintCallable, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject"), Category = "Dialogue")
		static UMessageQuickBinder* BindMessage(UObject* WorldContextObject, UDialogueEventObject* messageObj);

	virtual void Activate() override;
	
	UFUNCTION(BlueprintCallable, BlueprintPure, meta = (BlueprintInternalUseOnly = "true", WorldContext = "WorldContextObject"), Category = "Player Character")
	static APlayerController* GetPlayerController(UObject* WorldContextObject);

	UFUNCTION()
	void BeginMessageEvent(FName option);
	UFUNCTION()
	void EndMessageEvent(FName option);

private:
	UObject* WorldContextObject;
	UDialogueEventObject* messageObj;
};
