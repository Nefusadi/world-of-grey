// Fill out your copyright notice in the Description page of Project Settings.

#include "WorldOfGrey.h"
#include "MessageQuickBinder.h"

UMessageQuickBinder::UMessageQuickBinder(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
	, WorldContextObject(nullptr)
{
}

UMessageQuickBinder* UMessageQuickBinder::BindMessage(UObject* WorldContextObject, UDialogueEventObject* messageObj)
{
	UMessageQuickBinder* Proxy = NewObject<UMessageQuickBinder>();
	Proxy->messageObj = messageObj;
	return Proxy;
}

void UMessageQuickBinder::Activate()
{
	if (messageObj == nullptr)
		return;

	messageObj->OnBegin.AddDynamic(this, &UMessageQuickBinder::BeginMessageEvent);
	messageObj->OnEnd.AddDynamic(this, &UMessageQuickBinder::EndMessageEvent);
}

void UMessageQuickBinder::BeginMessageEvent(FName option)
{
	MessageBegin.Broadcast(option);
}

void UMessageQuickBinder::EndMessageEvent(FName option)
{
	MessageEnd.Broadcast(option);
}

APlayerController* UMessageQuickBinder::GetPlayerController(UObject* WorldContextObject)
{
	APlayerController* PC = WorldContextObject->GetWorld()->GetFirstPlayerController();
	return PC;
}