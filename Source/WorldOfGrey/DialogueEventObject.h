// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "DialogueEventObject.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FMessageEvent, FName, option);

/**
 * 
 */
UCLASS(Blueprintable)
class WORLDOFGREY_API UDialogueEventObject : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable)
	FMessageEvent OnBegin;

	UPROPERTY(BlueprintAssignable)
	FMessageEvent OnEnd;

	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"), Category = "Dialogue")
		void BeginMessage(UObject* WorldContextObject);

	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"), Category = "Dialogue")
		void EndMessage(UObject* WorldContextObject, FName option);
};
