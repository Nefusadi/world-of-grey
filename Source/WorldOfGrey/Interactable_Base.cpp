// Fill out your copyright notice in the Description page of Project Settings.

#include "WorldOfGrey.h"
#include "Interactable_Base.h"

// Sets default values
AInteractable_Base::AInteractable_Base()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AInteractable_Base::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AInteractable_Base::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AInteractable_Base::DefaultUseItem_Implementation()
{

}

void AInteractable_Base::SetItemFunction(EItemID itemID, const FItemFunction& func)
{
	while ((uint8)itemID >= (uint8)itemFuncMapping.Num())
		itemFuncMapping.Add(FItemFunction());
	itemFuncMapping[(uint8)itemID] = func;
}

void AInteractable_Base::ItemInteract(EItemID itemID)
{
	if ((uint8)itemID > 0 && (uint8)itemID < itemFuncMapping.Num() && itemFuncMapping[(uint8)itemID].IsBound())
		itemFuncMapping[(uint8)itemID].Execute();
	else
		DefaultUseItem();
}

