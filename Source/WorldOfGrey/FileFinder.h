// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "FileFinder.generated.h"

/**
 * 
 */
UCLASS()
class WORLDOFGREY_API UFileFinder : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:

	/** Obtain all files in a provided directory, with optional extension filter. All files are returned if Ext is left blank. Returns false if operation could not occur. */
	UFUNCTION(BlueprintPure, Category = "File IO")
		static bool GetSaveFiles(TArray<FString>& Files);
};
