// Fill out your copyright notice in the Description page of Project Settings.

#include "WorldOfGrey.h"
#include "FileFinder.h"

bool UFileFinder::GetSaveFiles(TArray<FString>& Files)
{
	FString RootFolderFullPath = FPaths::GameSavedDir() + "/SaveGames";

	FPaths::NormalizeDirectoryName(RootFolderFullPath);

	IFileManager& FileManager = IFileManager::Get();

	FString Ext = "*.sav";

	FString FinalPath = RootFolderFullPath + "/" + Ext;
	FileManager.FindFiles(Files, *FinalPath, true, false);
	return true;
}