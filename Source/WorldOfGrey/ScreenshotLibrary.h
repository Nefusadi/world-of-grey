// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "ScreenshotLibrary.generated.h"

/**
 * 
 */
UCLASS()
class WORLDOFGREY_API UScreenshotLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintPure, Category="CustomScreenshots")
		static UTexture2D* LoadScreenshot(const FString& filename);

	UFUNCTION(BlueprintPure, Category = "CustomScreenshots")
		static FString GetScreenshotDir();
	
	UFUNCTION(BlueprintCallable, Category = "CustomScreenshots")
		static bool SaveRenderTargetToDisk(UTextureRenderTarget2D* InRenderTarget, FString Filename);

	UFUNCTION(BlueprintCallable, Category = "CustomScreenshots")
		static bool DeleteScreenshot(const FString& filename);
};
