// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "ItemID.h"
#include "Interactable_Base.generated.h"

DECLARE_DYNAMIC_DELEGATE(FItemFunction);

UCLASS()
class WORLDOFGREY_API AInteractable_Base : public AActor
{
	GENERATED_BODY()
private:

	TArray<FItemFunction> itemFuncMapping;
	
public:	
	// Sets default values for this actor's properties
	AInteractable_Base();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(BlueprintNativeEvent, Category = "ItemInteractions")
		void DefaultUseItem();
		virtual void DefaultUseItem_Implementation();

	UFUNCTION(BlueprintCallable, Category = "ItemInteractions")
		void SetItemFunction(EItemID itemID, const FItemFunction& func);
	
	UFUNCTION(BlueprintCallable, Category = "ItemInteractions")
		void ItemInteract(EItemID itemID);
};
